# -*- coding: utf-8 -*-
"""
Copyright (c) 2011-2019 TONIOLO PIERPAOLO, All rights reserved.
Use of this source code is governed by a BSD-style license that can be
found in the LICENSE file.

This file is part of Propilei project
-------------------------------------
Created on 9 gen 2019

@author: tecnosegugio
"""

from django.db import models


class AuthGroup(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=80)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthUser(models.Model):
    id = models.AutoField(primary_key=True)
    username = models.CharField(max_length=150)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.CharField(max_length=254)
    password = models.CharField(max_length=128)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    is_superuser = models.BooleanField()
    last_login = models.DateTimeField(blank=True, null=True)
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey('propilei.AuthUser', on_delete=models.PROTECT)
    group = models.ForeignKey('propilei.AuthGroup', on_delete=models.PROTECT)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'


class ComuniComune(models.Model):
    id = models.AutoField(primary_key=True)
    nome = models.CharField(max_length=255)
    cap = models.CharField(max_length=5)
    provincia = models.ForeignKey(
        'propilei.ComuniProvincia', on_delete=models.PROTECT)
    prefisso = models.CharField(max_length=6, blank=True, null=True)
    istat = models.CharField(max_length=6, blank=True, null=True)
    codfisco = models.CharField(max_length=4, blank=True, null=True)
    link = models.CharField(max_length=200, blank=True, null=True)
    fullname = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'comuni_comune'


class ComuniProvincia(models.Model):
    id = models.AutoField(primary_key=True)
    codice = models.CharField(max_length=2)
    nome = models.CharField(max_length=255)
    regione = models.ForeignKey(
        'propilei.ComuniRegione', on_delete=models.PROTECT)

    class Meta:
        managed = False
        db_table = 'comuni_provincia'


class ComuniRegione(models.Model):
    id = models.AutoField(primary_key=True)
    codice = models.CharField(max_length=3)
    nome = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'comuni_regione'


class CoreAgenzia(models.Model):
    id = models.AutoField(primary_key=True)
    nome = models.CharField(max_length=255)
    compagnia1 = models.ForeignKey(
        'propilei.CoreCompagnia', on_delete=models.PROTECT
    )
    fullname = models.CharField(max_length=255)
    codice = models.CharField(max_length=10)
    indirizzo = models.CharField(max_length=255)
    citta = models.CharField(max_length=255)
    cap = models.CharField(max_length=5)
    provincia = models.CharField(max_length=2)
    nazione = models.CharField(max_length=255)
    telefono = models.CharField(max_length=255)
    fax = models.CharField(max_length=255)
    email = models.CharField(max_length=75)
    piva = models.CharField(max_length=11)
    referente = models.CharField(max_length=255)
    nota = models.TextField()
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    comune = models.ForeignKey(
        'propilei.ComuniComune', on_delete=models.PROTECT,
        blank=True, null=True
    )
    sostituito_da = models.ForeignKey(
        'propilei.CoreAgenzia', on_delete=models.PROTECT,
        blank=True, null=True
    )
    sostituito_il = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'core_agenzia'


class CoreAgenziaperiti(models.Model):
    id = models. AutoField(primary_key=True)
    agenzia = models.ForeignKey(
        'propilei.CoreAgenzia', on_delete=models.PROTECT
    )
    perito = models.ForeignKey('AuthUser', on_delete=models.PROTECT)

    class Meta:
        managed = False
        db_table = 'core_agenziaperiti'


class CoreCompagnia(models.Model):
    id = models.AutoField(primary_key=True)
    nome = models.CharField(max_length=255)
    ania = models.CharField(max_length=4)
    indirizzo = models.CharField(max_length=512)
    citta = models.CharField(max_length=255)
    cap = models.CharField(max_length=5)
    provincia = models.CharField(max_length=2)
    nazione = models.CharField(max_length=255)
    piva = models.CharField(max_length=11)
    email = models.CharField(max_length=75)
    ggconsegna = models.IntegerField(blank=True, null=True)
    ggsopralluogo = models.IntegerField(blank=True, null=True)
    restituzione = models.CharField(max_length=2)
    url_restituzione = models.CharField(max_length=200)
    email_restituzione = models.CharField(max_length=75)
    atto_liquidazione = models.CharField(max_length=2)
    nota = models.TextField()
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    comune = models.ForeignKey(
        'propilei.ComuniComune', on_delete=models.PROTECT,
        blank=True, null=True
    )
    # Field name made lowercase.
    datasufoto = models.BooleanField(db_column='dataSuFoto')
    # Field name made lowercase.
    richiedeinterlocutoria = models.BooleanField(
        db_column='richiedeInterlocutoria')
    attiva = models.BooleanField()
    # Field name made lowercase.
    ggprimocontatto = models.IntegerField(
        db_column='ggprimoContatto', blank=True, null=True)
    # Field name made lowercase.
    gginterlocutoria = models.IntegerField(
        db_column='ggInterlocutoria', blank=True, null=True)
    # Field name made lowercase.
    ggattesamancatasottoscrizione = models.IntegerField(
        db_column='ggAttesaMancataSottoscrizione', blank=True, null=True)
    # Field name made lowercase.
    limitepl = models.DecimalField(
        db_column='limitePL', max_digits=6, decimal_places=2)

    class Meta:
        managed = False
        db_table = 'core_compagnia'


class CoreCompagniaCollegate(models.Model):
    id = models.AutoField(primary_key=True)
    from_compagnia = models.ForeignKey(
        'propilei.CoreCompagnia', on_delete=models.PROTECT,
        related_name='from_compagnia_set',
    )
    to_compagnia = models.ForeignKey(
        'propilei.CoreCompagnia', on_delete=models.PROTECT,
        related_name='to_compagnia_set',
    )

    class Meta:
        managed = False
        db_table = 'core_compagnia_collegate'


class CoreCompagniaagenzie(models.Model):
    id = models.AutoField(primary_key=True)
    compagnia = models.ForeignKey(
        'propilei.CoreCompagnia', on_delete=models.PROTECT
    )
    agenzia = models.ForeignKey(
        'propilei.CoreAgenzia', on_delete=models.PROTECT)

    class Meta:
        managed = False
        db_table = 'core_compagniaagenzie'


class CoreComputo(models.Model):
    id = models.AutoField(primary_key=True)
    incarico = models.ForeignKey(
        'propilei.CoreIncarico', on_delete=models.PROTECT)
    posizione = models.IntegerField(blank=True, null=True)
    garanzia = models.ForeignKey(
        'propilei.CoreGaranzia', on_delete=models.PROTECT)
    partita = models.ForeignKey(
        'propilei.CorePartita', on_delete=models.PROTECT)
    degrado_partita = models.IntegerField(blank=True, null=True)
    degrado_valore = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    limite_indennizzo = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    totnuovo = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    totale = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    totnuovoivato = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    totusoivato = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    importo_iva_nuovo = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    importo_iva_uso = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    degrado_ivato = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    totoriginale = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    importo_iva_originale = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    totoriginaleivato = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'core_computo'


class CoreComputoriga(models.Model):
    id = models.AutoField(primary_key=True)
    computo = models.ForeignKey(
        'propilei.CoreComputo', on_delete=models.PROTECT)
    posizione = models.IntegerField(blank=True, null=True)
    voce = models.TextField(blank=True, null=True)
    quantita = models.DecimalField(max_digits=12, decimal_places=2)
    umisura = models.CharField(max_length=10, blank=True, null=True)
    prezzo = models.DecimalField(max_digits=12, decimal_places=2)
    degrado_voce = models.IntegerField(blank=True, null=True)
    degrado_valore = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    totnuovo = models.DecimalField(max_digits=12, decimal_places=2)
    totriga = models.DecimalField(max_digits=12, decimal_places=2)
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    aliquota_iva = models.IntegerField(blank=True, null=True)
    totnuovoivato = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    totrigaivato = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    degrado_ivato = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    tiporiga = models.CharField(max_length=2)

    class Meta:
        managed = False
        db_table = 'core_computoriga'


class CoreDocumento(models.Model):
    id = models.AutoField(primary_key=True)
    incarico = models.ForeignKey(
        'propilei.CoreIncarico', on_delete=models.PROTECT)
    tipo = models.ForeignKey('propilei.CoreTipodoc', on_delete=models.PROTECT)
    descrizione = models.CharField(max_length=255)
    da_assicurato = models.BooleanField()
    content = models.CharField(max_length=512)
    data_documento = models.DateField()
    versione = models.IntegerField()
    nota = models.TextField()
    allegato_perizia = models.BooleanField()
    data_consegna = models.DateField(blank=True, null=True)
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    approvato = models.BooleanField()
    consegnato = models.BooleanField()
    messaggio = models.ForeignKey(
        'propilei.CoreMessaggio', on_delete=models.PROTECT,
        blank=True, null=True
    )
    posizione = models.IntegerField(blank=True, null=True)
    content_type = models.CharField(max_length=255, blank=True, null=True)
    macchina_fotografica = models.CharField(max_length=511)
    data_foto = models.DateField(blank=True, null=True)
    latitudine = models.DecimalField(
        max_digits=24, decimal_places=20, blank=True, null=True)
    longitudine = models.DecimalField(
        max_digits=24, decimal_places=20, blank=True, null=True)
    # Field name made lowercase.
    data_stampafoto = models.DateField(
        db_column='data_stampaFoto', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'core_documento'


class CoreFattura(models.Model):
    id = models.AutoField(primary_key=True)
    numero_fattura = models.ForeignKey(
        'propilei.CoreFatturaNumero', on_delete=models.PROTECT)
    data_fattura = models.DateField()
    cliente_nome = models.CharField(max_length=255)
    cliente_indirizzo = models.CharField(max_length=512)
    cliente_citta = models.CharField(max_length=255)
    cliente_cap = models.CharField(max_length=5)
    cliente_provincia = models.CharField(max_length=2)
    cliente_piva = models.CharField(max_length=11)
    importo_onorario = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    importo_km = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    importo_foto = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    importo_varie = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    imponibile = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    aliquota_iva = models.IntegerField(blank=True, null=True)
    importo_iva = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    totale = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    compagnia = models.ForeignKey(
        'propilei.CoreCompagnia', on_delete=models.PROTECT,
        blank=True, null=True
    )
    chiusa = models.BooleanField()
    annotazioni = models.TextField()
    user_create = models.IntegerField()
    incarico_compagnia = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'core_fattura'


class CoreFatturanumero(models.Model):
    id = models.AutoField(primary_key=True)
    organizzazione = models.ForeignKey(
        'propilei.CoreOrganizzazione', on_delete=models.PROTECT)
    numero = models.IntegerField()
    anno = models.IntegerField()
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    data_fattura = models.DateField(blank=True, null=True)
    user_create = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'core_fatturanumero'


class CoreFatturariga(models.Model):
    id = models.AutoField(primary_key=True)
    fattura = models.ForeignKey(
        'propilei.CoreFattura', on_delete=models.PROTECT)
    repertorio = models.CharField(max_length=10)
    incarico = models.ForeignKey(
        'propilei.CoreIncarico', on_delete=models.PROTECT,
        blank=True, null=True)
    sincro_netelenco = models.FloatField(blank=True, null=True)
    imponibile = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    onorario = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    onorario_foto = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    onorario_km = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    onorario_varie = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    perito_onorario = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    perito_onorario_foto = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    perito_onorario_km = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    codice_agenzia = models.CharField(max_length=10, blank=True, null=True)
    num_sinistro = models.CharField(max_length=255, blank=True, null=True)
    data_sinistro = models.DateField(blank=True, null=True)
    assicurato = models.CharField(max_length=255, blank=True, null=True)
    danneggiato1 = models.CharField(max_length=255, blank=True, null=True)
    num_polizza = models.CharField(max_length=255, blank=True, null=True)
    perito_imponibile = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    perito = models.ForeignKey(
        'propilei.AuthUser', on_delete=models.PROTECT,
        related_name='fatturariga_perito',
    )
    collaboratore = models.CharField(max_length=40)
    data_fattura = models.DateField(blank=True, null=True)
    organizzazione = models.ForeignKey(
        'propilei.CoreOrganizzazione', on_delete=models.PROTECT)
    revisore = models.ForeignKey(
        'propilei.AuthUser', on_delete=models.PROTECT,
        related_name='fatturariga_revisore',
    )
    revisore_imponibile = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    perito_provv_perc = models.DecimalField(
        max_digits=5, decimal_places=1, blank=True, null=True)
    data_incarico = models.DateField(blank=True, null=True)
    data_restituzione = models.DateField(blank=True, null=True)
    posizione = models.IntegerField(blank=True, null=True)
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    tipo = models.CharField(max_length=2)
    descrizione = models.CharField(max_length=512, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'core_fatturariga'


class CoreGaranzia(models.Model):
    id = models.AutoField(primary_key=True)
    incarico = models.ForeignKey(
        'propilei.CoreIncarico', on_delete=models.PROTECT)
    interessata = models.BooleanField()
    tipo = models.ForeignKey(
        'propilei.CoreTipogaranzia', on_delete=models.PROTECT)
    altro_tipo = models.CharField(max_length=255, blank=True, null=True)
    tipo_sinistro = models.ForeignKey(
        'propilei.CoreTiposinistro', on_delete=models.PROTECT,
        blank=True, null=True)
    forma_copertura = models.CharField(max_length=3)
    franchigia = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    limite_perc = models.FloatField(blank=True, null=True)
    limite_valore = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    limite_min = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    limite_max = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    limite_anno_perc = models.FloatField(blank=True, null=True)
    limite_anno_valore = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    limite_anno_min = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    limite_anno_max = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    scoperto_perc = models.FloatField(blank=True, null=True)
    scoperto_valore = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    scoperto_min = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    scoperto_max = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    descrizione = models.TextField()
    certificata = models.BooleanField()
    certificatore = models.ForeignKey(
        'propilei.AuthUser', on_delete=models.PROTECT,
        blank=True, null=True)
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    limite_perc_tipo = models.CharField(max_length=4, blank=True, null=True)
    limite_anno_perc_tipo = models.CharField(
        max_length=4, blank=True, null=True)
    somma_erogata_anno = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    salta_scopertura = models.BooleanField()
    posizione = models.IntegerField(blank=True, null=True)
    franchigia_tipo = models.CharField(max_length=2)

    class Meta:
        managed = False
        db_table = 'core_garanzia'


class CoreIncarico(models.Model):
    id = models.AutoField(primary_key=True)
    repertorio = models.BigIntegerField()
    stato = models.ForeignKey(
        'propilei.CoreStato',
        to_field='codice',
        on_delete=models.PROTECT)
    compagnia = models.ForeignKey(
        'propilei.CoreCompagnia', on_delete=models.PROTECT,
        blank=True, null=True)
    agenzia = models.ForeignKey(
        'propilei.CoreAgenzia', on_delete=models.PROTECT)
    organizzazione = models.ForeignKey(
        'propilei.CoreOrganizzazione', on_delete=models.PROTECT,
        related_name='incarico_organizzazione',
    )
    perito = models.ForeignKey(
        'propilei.AuthUser', on_delete=models.PROTECT,
        blank=True, null=True
    )
    revisore = models.ForeignKey(
        'propilei.AuthUser', on_delete=models.PROTECT,
        blank=True, null=True,
        related_name='incarico_revisore',
    )
    assicurato = models.ForeignKey(
        'propilei.CorePersona', on_delete=models.PROTECT,
        related_name='incarico_assicurato',
    )
    contraente = models.ForeignKey(
        'propilei.CorePersona', on_delete=models.PROTECT,
        blank=True, null=True,
        related_name='incarico_contraente',
    )
    danneggiato1 = models.ForeignKey(
        'propilei.CorePersona', on_delete=models.PROTECT,
        blank=True, null=True,
        related_name='incarico_danneggiato1',
    )
    danneggiato2 = models.ForeignKey(
        'propilei.CorePersona', on_delete=models.PROTECT,
        blank=True, null=True,
        related_name='incarico_danneggiato2',
    )
    danneggiato3 = models.ForeignKey(
        'propilei.CorePersona', on_delete=models.PROTECT,
        blank=True, null=True,
        related_name='incarico_danneggiato3',
    )
    danneggiato4 = models.ForeignKey(
        'propilei.CorePersona', on_delete=models.PROTECT,
        blank=True, null=True,
        related_name='incarico_danneggiato4',
    )
    danneggiato5 = models.ForeignKey(
        'propilei.CorePersona', on_delete=models.PROTECT,
        blank=True, null=True,
        related_name='incarico_danneggiato5',
    )
    ispettorato = models.ForeignKey(
        'propilei.CoreIspettorato', on_delete=models.PROTECT,
    )
    data_sinistro = models.DateField(blank=True, null=True)
    data_denuncia = models.DateField(blank=True, null=True)
    data_assegnazione = models.DateField(blank=True, null=True)
    data_assegnazione_perito = models.DateField(blank=True, null=True)
    data_sopralluogo = models.DateField(blank=True, null=True)
    data_revisione = models.DateField(blank=True, null=True)
    data_restituzione = models.DateField(blank=True, null=True)
    data_fatturazione = models.DateField(blank=True, null=True)
    mancato_sopralluogo = models.CharField(max_length=4)
    ubicazione_rischio = models.TextField()
    tipo_sinistro = models.ForeignKey(
        'propilei.CoreTiposinistro', on_delete=models.PROTECT)
    num_sinistro = models.CharField(max_length=255)
    indirizzo_sinistro = models.CharField(max_length=512)
    cap_sinistro = models.CharField(max_length=5)
    citta_sinistro = models.CharField(max_length=255)
    provincia_sinistro = models.CharField(max_length=2)
    nazione_sinistro = models.CharField(max_length=255)
    luogo_appuntamento = models.TextField()
    data_appuntamento = models.DateTimeField(blank=True, null=True)
    num_polizza = models.CharField(max_length=255)
    libretto = models.ForeignKey(
        'propilei.CoreLibretto', on_delete=models.PROTECT,
        blank=True, null=True)
    riferimento = models.CharField(max_length=255)
    ramo_polizza = models.ForeignKey(
        'propilei.CoreRamo', on_delete=models.PROTECT, blank=True, null=True)
    pl_pagamento = models.CharField(max_length=4)
    coassicuratrici = models.TextField()
    sinistro_complesso = models.BooleanField()
    vincoli = models.BooleanField()
    riserve = models.BooleanField()
    tipo_atto = models.CharField(max_length=2)
    contatto_persona = models.CharField(max_length=255)
    contatto_telefono = models.CharField(max_length=255)
    data_primo_contatto = models.DateTimeField(blank=True, null=True)
    data_effetto = models.DateField(blank=True, null=True)
    data_scadenza_premio = models.DateField(blank=True, null=True)
    data_pagamento_premio = models.DateField(blank=True, null=True)
    data_scadenza_copertura = models.DateField(blank=True, null=True)
    durata_copertura = models.CharField(max_length=2)
    franchigia_frontale = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    deroga_globale = models.IntegerField(blank=True, null=True)
    tipo_deroga = models.CharField(max_length=4)
    danno_nuovo = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    danno_lordo = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    danno_indennizzabile = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    danno_arrotondato = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    liq_calcolata_stima = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    liq_calcolata_supplemento = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    liq_calcolata_totale = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    liq_arrotondata_stima = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    liq_arrotondata_supplemento = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    liq_arrotondata_totale = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    onorario = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    perito_provv_fisso = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    perito_provv_perc = models.DecimalField(
        max_digits=5, decimal_places=1, blank=True, null=True)
    revisore_provv_fisso = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    revisore_provv_perc = models.DecimalField(
        max_digits=5, decimal_places=1, blank=True, null=True)
    provv_perito = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    provv_revisore = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    compenso = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    onorario_foto = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    onorario_km = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    onorario_varie = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    azienda_fattura = models.ForeignKey(
        'propilei.CoreOrganizzazione', on_delete=models.PROTECT,
        blank=True, null=True)
    onorario_km_percorsi = models.IntegerField(blank=True, null=True)
    onorario_km_franchigia = models.IntegerField(blank=True, null=True)
    onorario_km_costo = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    onorario_foto_numero = models.IntegerField(blank=True, null=True)
    onorario_foto_franchigia = models.IntegerField(blank=True, null=True)
    onorario_foto_costo = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    perito_onorario_foto_numero = models.IntegerField(blank=True, null=True)
    perito_onorario_foto_franchigia = models.IntegerField(
        blank=True, null=True)
    perito_onorario_foto_costo = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    perito_onorario_foto = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    perito_onorario_km_percorsi = models.IntegerField(blank=True, null=True)
    perito_onorario_km_franchigia = models.IntegerField(blank=True, null=True)
    perito_onorario_km_costo = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    perito_onorario_km = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    imponibile = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    perito_imponibile = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    perito_onorario = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    revisore_imponibile = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    riconosci_iva = models.BooleanField()
    riserva_importo = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    danno_originale = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    comune_sinistro = models.ForeignKey(
        'propilei.ComuniComune', on_delete=models.PROTECT,
        blank=True, null=True)
    aperto_pl = models.CharField(max_length=2)
    attesa = models.CharField(max_length=4)
    scadenza_attesa = models.DateField(blank=True, null=True)
    incarico_compagnia = models.CharField(
        max_length=255, blank=True, null=True)
    regolarita_amministrativa = models.CharField(
        max_length=2, blank=True, null=True)
    user_register = models.ForeignKey(
        'propilei.AuthUser', on_delete=models.PROTECT,
        blank=True, null=True,
        related_name='incarico_registratore',
    )
    user_deliver = models.ForeignKey(
        'propilei.AuthUser', on_delete=models.PROTECT, blank=True, null=True,
        related_name='incarico_restitutore',
    )
    data_registrazione = models.DateField(blank=True, null=True)
    iban = models.CharField(max_length=34)
    iban_intestatario = models.CharField(max_length=255)
    latitudine = models.DecimalField(
        max_digits=24, decimal_places=20, blank=True, null=True)
    longitudine = models.DecimalField(
        max_digits=24, decimal_places=20, blank=True, null=True)
    data_chiusura = models.DateField(blank=True, null=True)
    data_archiviazione = models.DateField(blank=True, null=True)
    rivalsa = models.BooleanField()
    note = models.TextField(blank=True, null=True)
    riparazione_diretta = models.BooleanField()
    video_perizia = models.BooleanField()

    class Meta:
        managed = False
        db_table = 'core_incarico'


class CoreIspettorato(models.Model):
    id = models.AutoField(primary_key=True)
    nome = models.CharField(max_length=255)
    compagnia = models.ForeignKey(
        'propilei.CoreCompagnia', on_delete=models.PROTECT
    )
    fullname = models.CharField(max_length=255)
    codice = models.CharField(max_length=10)
    indirizzo = models.CharField(max_length=255)
    citta = models.CharField(max_length=255)
    cap = models.CharField(max_length=5)
    provincia = models.CharField(max_length=2)
    nazione = models.CharField(max_length=255)
    telefono = models.CharField(max_length=255)
    fax = models.CharField(max_length=255)
    email = models.CharField(max_length=75)
    piva = models.CharField(max_length=11)
    referente = models.CharField(max_length=255)
    intestazione = models.CharField(max_length=4)
    nota = models.TextField()
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    comune = models.ForeignKey(
        'propilei.ComuniComune', on_delete=models.PROTECT,
        blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'core_ispettorato'


class CoreLibretto(models.Model):
    id = models.AutoField(primary_key=True)
    compagnia = models.ForeignKey(
        'propilei.CoreCompagnia', on_delete=models.PROTECT)
    content = models.CharField(max_length=512)
    nome = models.CharField(max_length=255)
    modello = models.CharField(max_length=255)
    versione = models.CharField(max_length=255)
    fullname = models.CharField(max_length=255)
    annotazioni = models.TextField(blank=True, null=True)
    franchigia_frontale = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    deroga_perc = models.IntegerField(blank=True, null=True)
    tipo_deroga = models.CharField(max_length=4)
    certificatore = models.ForeignKey(
        'propilei.AuthUser', on_delete=models.PROTECT)
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    content_type = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'core_libretto'


class CoreLibrettogaranzia(models.Model):
    id = models.AutoField(primary_key=True)
    libretto = models.ForeignKey(
        'propilei.CoreLibretto', on_delete=models.PROTECT)
    tipogaranzia = models.ForeignKey(
        'propilei.CoreTipogaranzia', on_delete=models.PROTECT)
    tipo_sinistro = models.ForeignKey(
        'propilei.CoreTiposinistro', on_delete=models.PROTECT,
        blank=True, null=True)
    forma_copertura = models.CharField(max_length=3)
    franchigia = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    limite_perc = models.FloatField(blank=True, null=True)
    limite_valore = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    limite_min = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    limite_max = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    limite_anno_perc = models.FloatField(blank=True, null=True)
    limite_anno_valore = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    limite_anno_min = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    limite_anno_max = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    scoperto_perc = models.IntegerField(blank=True, null=True)
    scoperto_valore = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    scoperto_min = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    scoperto_max = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    descrizione = models.TextField()
    certificatore = models.ForeignKey(
        'propilei.AuthUser', on_delete=models.PROTECT)
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    salta_scopertura = models.BooleanField()
    altro_tipogaranzia = models.CharField(
        max_length=255, blank=True, null=True)
    limite_perc_tipo = models.CharField(max_length=4, blank=True, null=True)
    limite_anno_perc_tipo = models.CharField(
        max_length=4, blank=True, null=True)
    franchigia_tipo = models.CharField(max_length=2)

    class Meta:
        managed = False
        db_table = 'core_librettogaranzia'


class CoreLiquidazione(models.Model):
    id = models.AutoField(primary_key=True)
    incarico = models.ForeignKey(
        'propilei.CoreIncarico', on_delete=models.PROTECT)
    garanzia = models.ForeignKey(
        'propilei.CoreGaranzia', on_delete=models.PROTECT)
    franchigia = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    totale_nuovo = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    totale_detrarre = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    totale_stima = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    totale_supplemento = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    totale_riga = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    scoperto_stima = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    scoperto_supplemento = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    scoperto_riga = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    importo_stima = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    importo_supplemento = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    importo_riga = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    totale_degrado = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    totale_proporzionale = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    importo_calcolato = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    importo_limite = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    limite_applicato = models.BooleanField()
    tot_liquidazione_altre = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    importo_limite_stima = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'core_liquidazione'


class CoreLiquidazioneriga(models.Model):
    id = models.AutoField(primary_key=True)
    liquidazione = models.ForeignKey(
        'propilei.CoreLiquidazione', on_delete=models.PROTECT)
    posizione = models.IntegerField(blank=True, null=True)
    partita = models.ForeignKey(
        'propilei.CorePartita', on_delete=models.PROTECT,
        blank=True, null=True)
    computo = models.ForeignKey(
        'propilei.CoreComputo', on_delete=models.PROTECT,
        blank=True, null=True)
    stima_nuovo = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    stima_degrado = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    stima_uso = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    stima_stato = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    somma_limite = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    valore_liquidazione = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    valore_supplemento = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    valore_proporzionale = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    totale_riga = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    totale_calcolato = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    limite_applicato = models.BooleanField()

    class Meta:
        managed = False
        db_table = 'core_liquidazioneriga'


class CoreMessaggio(models.Model):
    id = models.AutoField(primary_key=True)
    incarico = models.ForeignKey(
        'propilei.CoreIncarico', on_delete=models.PROTECT)
    subject = models.CharField(max_length=255)
    message_id = models.CharField(max_length=255)
    from_header = models.CharField(max_length=255)
    to_header = models.TextField()
    processed = models.DateTimeField(blank=True, null=True)
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    testo = models.TextField()
    result = models.TextField()

    class Meta:
        managed = False
        db_table = 'core_messaggio'


class CoreNonconforme(models.Model):
    id = models.AutoField(primary_key=True)
    data_respinta = models.DateTimeField(blank=True, null=True)
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    revisione = models.ForeignKey(
        'propilei.CoreRevisione', on_delete=models.PROTECT,
        blank=True, null=True)
    motivazione = models.TextField()
    utente_respintore = models.ForeignKey(
        'propilei.AuthUser', on_delete=models.PROTECT,
        blank=True, null=True,
        related_name='utente_respintore',
    )
    stato_nonconformita = models.ForeignKey(
        'propilei.CoreStato', on_delete=models.PROTECT,
        to_field='codice',
        blank=True, null=True)
    incarico = models.ForeignKey(
        'propilei.CoreIncarico', on_delete=models.PROTECT,
        blank=True, null=True)
    utente_respinto = models.ForeignKey(
        'propilei.AuthUser', on_delete=models.PROTECT,
        blank=True, null=True,
        related_name='utente_respinto',
    )

    class Meta:
        managed = False
        db_table = 'core_nonconforme'


class CoreOrganizzazione(models.Model):
    id = models.AutoField(primary_key=True)
    codice = models.CharField(max_length=10)
    nome = models.CharField(max_length=255)
    indirizzo = models.CharField(max_length=255, blank=True, null=True)
    citta = models.CharField(max_length=255)
    cap = models.CharField(max_length=5)
    provincia = models.CharField(max_length=2)
    nazione = models.CharField(max_length=255)
    telefono = models.CharField(max_length=255)
    fax = models.CharField(max_length=255)
    email = models.CharField(max_length=75)
    piva = models.CharField(max_length=11)
    referente = models.CharField(max_length=255)
    coordinatore = models.ForeignKey(
        'propilei.AuthUser', on_delete=models.PROTECT)
    nota = models.TextField()
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    org_provv_perc = models.DecimalField(max_digits=5, decimal_places=1)
    emissione_fattura = models.BooleanField()
    posizione = models.IntegerField(blank=True, null=True)
    is_active = models.BooleanField()
    comune = models.ForeignKey(
        'propilei.ComuniComune', on_delete=models.PROTECT,
        blank=True, null=True)
    verifica_perizie = models.BooleanField()

    class Meta:
        managed = False
        db_table = 'core_organizzazione'


class CoreOrganizzazioneCollaboratori(models.Model):
    id = models.AutoField(primary_key=True)
    organizzazione = models.ForeignKey(
        'propilei.CoreOrganizzazione', on_delete=models.PROTECT)
    user = models.ForeignKey('propilei.AuthUser', on_delete=models.PROTECT)

    class Meta:
        managed = False
        db_table = 'core_organizzazione_collaboratori'


class CorePartita(models.Model):
    id = models.AutoField(primary_key=True)
    incarico = models.ForeignKey(
        'propilei.CoreIncarico', on_delete=models.PROTECT)
    tipo = models.ForeignKey(
        'propilei.CoreTipopartita', on_delete=models.PROTECT)
    valore_ax_iniziale = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    valore_ax_indicizzato = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    valore_ax = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    valore_nuovo = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    deroga_non_applicata = models.BooleanField()
    deroga_perc = models.IntegerField(blank=True, null=True)
    deroga_valore = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    scopertura = models.BooleanField()
    scopertura_ratio = models.FloatField(blank=True, null=True)
    degrado = models.IntegerField(blank=True, null=True)
    valore_degrado = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    valore_uso = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    preesistenza_dichiarata = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    perc_danno = models.IntegerField(blank=True, null=True)
    forma_copertura = models.CharField(max_length=3)
    metodo_liquidazione = models.CharField(max_length=3)
    descrizione = models.CharField(max_length=255)
    colpita = models.BooleanField()
    preesistenza_opt = models.CharField(max_length=1)
    costruzione = models.CharField(max_length=50)
    costr_anno = models.IntegerField(blank=True, null=True)
    manutenzione = models.CharField(max_length=50)
    finiture = models.CharField(max_length=50)
    solai = models.CharField(max_length=50)
    piani_ft = models.IntegerField(blank=True, null=True)
    piani_int = models.IntegerField(blank=True, null=True)
    data_stima = models.DateField(blank=True, null=True)
    ubicazione_polizza = models.BooleanField()
    descrizione_polizza = models.BooleanField()
    destinazione = models.TextField()
    strutture = models.TextField()
    danno_accertato = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    danno_supplemento = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    danno_lordo = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    fabbricato_l1 = models.FloatField(blank=True, null=True)
    fabbricato_l2 = models.FloatField(blank=True, null=True)
    fabbricato_l3 = models.FloatField(blank=True, null=True)
    cubatura = models.FloatField(blank=True, null=True)
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    posizione = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'core_partita'


class CorePartitariga(models.Model):
    id = models.AutoField(primary_key=True)
    partita = models.ForeignKey(
        'propilei.CorePartita', on_delete=models.PROTECT)
    posizione = models.IntegerField(blank=True, null=True)
    descrizione = models.TextField(blank=True, null=True)
    fabbricato_l1 = models.FloatField(blank=True, null=True)
    fabbricato_l2 = models.FloatField(blank=True, null=True)
    fabbricato_l3 = models.FloatField(blank=True, null=True)
    fabbricato_cubatura = models.FloatField(blank=True, null=True)
    costo_mc = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    quantita = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    umisura = models.CharField(max_length=10, blank=True, null=True)
    prezzo_uni = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    valore_nuovo = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    degrado_riga = models.IntegerField(blank=True, null=True)
    valore_degrado = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    valore_uso = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'core_partitariga'


class CorePersona(models.Model):
    id = models.AutoField(primary_key=True)
    titolo = models.ForeignKey(
        'propilei.CoreTitolo', on_delete=models.PROTECT,
        blank=True, null=True)
    nome = models.CharField(max_length=255)
    giuridica = models.BooleanField()
    ruolo = models.CharField(max_length=3)
    cod_fiscale = models.CharField(max_length=16)
    piva = models.CharField(max_length=11)
    indirizzo = models.CharField(max_length=512)
    cap = models.CharField(max_length=5)
    citta = models.CharField(max_length=255)
    provincia = models.CharField(max_length=2)
    nazione = models.CharField(max_length=255)
    telefono1 = models.CharField(max_length=255)
    telefono2 = models.CharField(max_length=255)
    email = models.CharField(max_length=75)
    idcard = models.CharField(max_length=50)
    riferimento = models.CharField(max_length=255)
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    comune = models.ForeignKey(
        'propilei.ComuniComune', on_delete=models.PROTECT,
        blank=True, null=True)
    amministratore = models.ForeignKey(
        'propilei.CorePersona', on_delete=models.PROTECT,
        blank=True, null=True)
    iban = models.CharField(max_length=34)
    sostituito_da = models.ForeignKey(
        'propilei.CorePersona', on_delete=models.PROTECT,
        blank=True, null=True,
        related_name='sostituisce_set',
    )
    sostituito_il = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'core_persona'


class CoreRamo(models.Model):
    id = models.AutoField(primary_key=True)
    descrizione = models.CharField(max_length=255)
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'core_ramo'


class CoreRelazione(models.Model):
    id = models.AutoField(primary_key=True)
    incarico = models.ForeignKey(
        'propilei.CoreIncarico', on_delete=models.PROTECT)
    sezione01 = models.TextField()
    sezione02 = models.TextField()
    sezione03 = models.TextField()
    sezione04 = models.TextField()
    sezione05 = models.TextField()
    sezione06 = models.TextField()
    sezione07 = models.TextField()
    sezione08 = models.TextField()
    sezione09 = models.TextField()
    sezione10 = models.TextField()
    sezione11 = models.TextField()
    sezione12 = models.TextField()
    foglio_liquidazione = models.TextField()
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'core_relazione'


class CoreRevisione(models.Model):
    id = models.AutoField(primary_key=True)
    incarico = models.ForeignKey(
        'propilei.CoreIncarico', on_delete=models.PROTECT)
    sezione01 = models.TextField()
    sezione02 = models.TextField()
    sezione03 = models.TextField()
    sezione04 = models.TextField()
    sezione05 = models.TextField()
    sezione06 = models.TextField()
    sezione07 = models.TextField()
    sezione08 = models.TextField()
    sezione09 = models.TextField()
    sezione10 = models.TextField()
    sezione11 = models.TextField()
    sezione12 = models.TextField()
    foglio_liquidazione = models.TextField()
    commenti01 = models.TextField()
    commenti02 = models.TextField()
    commenti03 = models.TextField()
    commenti04 = models.TextField()
    commenti05 = models.TextField()
    commenti06 = models.TextField()
    commenti07 = models.TextField()
    commenti08 = models.TextField()
    commenti09 = models.TextField()
    commenti10 = models.TextField()
    commenti11 = models.TextField()
    commenti12 = models.TextField()
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    revisore_attuale = models.ForeignKey(
        'propilei.AuthUser', on_delete=models.PROTECT,
        blank=True, null=True)
    revisore_apertura = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'core_revisione'


class CoreSopralluogo(models.Model):
    id = models.AutoField(primary_key=True)
    incarico = models.ForeignKey(
        'propilei.CoreIncarico', on_delete=models.PROTECT)
    # Field name made lowercase.
    personasopralluogo = models.CharField(
        db_column='personaSopralluogo', max_length=255)
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'core_sopralluogo'


class CoreStato(models.Model):
    id = models.AutoField(primary_key=True)
    codice = models.CharField(max_length=2, unique=True)
    descrizione = models.CharField(max_length=50)
    next_stato = models.CharField(max_length=2)
    colore = models.CharField(max_length=7)
    coltxt = models.CharField(max_length=7)
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'core_stato'


class CoreTipodoc(models.Model):
    id = models.AutoField(primary_key=True)
    codice = models.CharField(max_length=4)
    descrizione = models.CharField(max_length=50)
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    colore = models.CharField(max_length=7)
    modulo = models.BooleanField()
    corrispondenza = models.BooleanField()
    allegabile = models.BooleanField()
    parole_chiave = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'core_tipodoc'


class CoreTipogaranzia(models.Model):
    id = models.AutoField(primary_key=True)
    codice = models.CharField(max_length=6)
    nome = models.CharField(max_length=255)
    descrizione = models.TextField()
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'core_tipogaranzia'


class CoreTipopartita(models.Model):
    id = models.AutoField(primary_key=True)
    codice = models.CharField(max_length=4)
    nome = models.CharField(max_length=255)
    descrizione = models.TextField(blank=True, null=True)
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'core_tipopartita'


class CoreTipoprezziario(models.Model):
    id = models.AutoField(primary_key=True)
    nome = models.CharField(max_length=255)
    descrizione = models.TextField(blank=True, null=True)
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'core_tipoprezziario'


class CoreTiposinistro(models.Model):
    id = models.AutoField(primary_key=True)
    codice = models.CharField(max_length=4)
    descrizione = models.CharField(max_length=255)
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    istruzioni = models.TextField()

    class Meta:
        managed = False
        db_table = 'core_tiposinistro'


class CoreUserprofile(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey('propilei.AuthUser', on_delete=models.PROTECT)
    titolo = models.ForeignKey(
        'propilei.CoreTitolo', on_delete=models.PROTECT,
        blank=True, null=True)
    indirizzo = models.CharField(max_length=512)
    cap = models.CharField(max_length=5)
    citta = models.CharField(max_length=255)
    nazione = models.CharField(max_length=255)
    telefono1 = models.CharField(max_length=255)
    telefono2 = models.CharField(max_length=255)
    piva = models.CharField(max_length=11)
    cod_fiscale = models.CharField(max_length=16)
    perito_capacita = models.IntegerField(blank=True, null=True)
    perito_carico = models.IntegerField(blank=True, null=True)
    perito_provv_fisso = models.DecimalField(max_digits=12, decimal_places=2)
    perito_provv_perc = models.DecimalField(max_digits=5, decimal_places=1)
    revisore_provv_fisso = models.DecimalField(max_digits=12, decimal_places=2)
    revisore_provv_perc = models.DecimalField(max_digits=5, decimal_places=1)
    img_firma = models.CharField(max_length=512, blank=True, null=True)
    liquidato_medio = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    elaborato_medio = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    email2 = models.CharField(max_length=75)
    email3 = models.CharField(max_length=75)
    iban = models.CharField(max_length=34)
    cattolica_userid = models.CharField(max_length=64)

    class Meta:
        managed = False
        db_table = 'core_userprofile'


class CoreTitolo(models.Model):
    id = models.AutoField(primary_key=True)
    titolo = models.CharField(max_length=15)
    descrizione = models.CharField(max_length=64)
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'core_titolo'


class InterlocutorieInterlocutoria(models.Model):
    id = models.AutoField(primary_key=True)
    incarico = models.ForeignKey(
        'propilei.CoreIncarico', on_delete=models.PROTECT)
    data_documento = models.DateField()
    tipo = models.CharField(max_length=4)
    sezione01 = models.TextField()
    sezione02 = models.TextField()
    sezione03 = models.TextField()
    sezione04 = models.TextField()
    data_consegna = models.DateField(blank=True, null=True)
    posizione = models.IntegerField(blank=True, null=True)
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    riserva_valore = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    gallery = models.BooleanField()
    stato = models.CharField(max_length=2)
    formato = models.CharField(max_length=3)
    documento = models.ForeignKey(
        'propilei.CoreDocumento', on_delete=models.PROTECT,
        blank=True, null=True)
    esterno = models.BooleanField()

    class Meta:
        managed = False
        db_table = 'interlocutorie_interlocutoria'
