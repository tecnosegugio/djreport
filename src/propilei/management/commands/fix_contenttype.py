# -*- coding: UTF-8 -*-
"""
Copyright (c) 2011-2019 TONIOLO PIERPAOLO, All rights reserved.
Use of this source code is governed by a BSD-style license that can be
found in the LICENSE file.

This file is part of Propilei project
-------------------------------------
Created on 10 gen 2019

@author: tecnosegugio
"""

import sys

from django.contrib.contenttypes.models import ContentType
from django.core.management.base import BaseCommand
from django.apps import apps


class Command(BaseCommand):
    help = "Fix ContentType for unmanaged models."

    def handle(self, *args, **options):
        for model in apps.get_models():
            opts = model._meta
            # sys.stdout.write(
            #    '{}-{}\n'.format(opts.app_label, opts.object_name.lower())
            # )
            ctype, created = ContentType.objects.get_or_create(
                app_label=opts.app_label,
                model=opts.object_name.lower())
            if created:
                sys.stdout.write(
                    'Added ContentType {} for App Label {} model {}\n'.format(
                        ctype, opts.app_label, opts.object_name.lower()
                    )
                )

            # Il codice qui sotto serve per aggiungere anch ele permission
            # ai Model appena aggiunti a ContenType
#             for codename, name in _get_all_permissions(opts):
#                 sys.stdout.write('  --{}\n'.format(codename))
#                 p, created = Permission.objects.get_or_create(
#                     codename=codename,
#                     content_type=ctype,
#                     defaults={'name': name}
#                 )
