from django.apps import AppConfig


class PropileiConfig(AppConfig):
    name = 'propilei'
