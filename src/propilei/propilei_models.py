# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or
# field names.
from django.db import models


class AdminToolsDashboardPreferences(models.Model):
    id = models.AutoField()
    user_id = models.IntegerField()
    data = models.TextField()
    dashboard_id = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'admin_tools_dashboard_preferences'


class AdminToolsMenuBookmark(models.Model):
    id = models.AutoField()
    user_id = models.IntegerField()
    url = models.CharField(max_length=255)
    title = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'admin_tools_menu_bookmark'


class AppmobileQpagina(models.Model):
    id = models.AutoField()
    nome = models.CharField(max_length=64)
    posizione = models.IntegerField()
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    descrizione = models.CharField(max_length=64, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'appmobile_qpagina'


class AppmobileQpaginaTipiSinistro(models.Model):
    id = models.AutoField()
    qpagina_id = models.IntegerField()
    tiposinistro_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'appmobile_qpagina_tipi_sinistro'


class AppmobileQrisposte(models.Model):
    id = models.AutoField()
    # This field type is a guess.
    risposte = models.TextField(blank=True, null=True)
    data_inserimento = models.DateTimeField()
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    incarico_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'appmobile_qrisposte'


class AppmobileQvoce(models.Model):
    id = models.AutoField()
    campo = models.CharField(max_length=64)
    posizione = models.IntegerField()
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    pagina_id = models.IntegerField()
    obbligatorio = models.BooleanField()

    class Meta:
        managed = False
        db_table = 'appmobile_qvoce'


class AuthGroup(models.Model):
    id = models.AutoField()
    name = models.CharField(max_length=80)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    id = models.AutoField()
    group_id = models.IntegerField()
    permission_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'


class AuthMessage(models.Model):
    id = models.AutoField()
    user_id = models.IntegerField()
    message = models.TextField()

    class Meta:
        managed = False
        db_table = 'auth_message'


class AuthPermission(models.Model):
    id = models.AutoField()
    name = models.CharField(max_length=255)
    content_type_id = models.IntegerField()
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'


class AuthUser(models.Model):
    id = models.AutoField()
    username = models.CharField(max_length=150)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.CharField(max_length=254)
    password = models.CharField(max_length=128)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    is_superuser = models.BooleanField()
    last_login = models.DateTimeField(blank=True, null=True)
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    id = models.AutoField()
    user_id = models.IntegerField()
    group_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'auth_user_groups'


class AuthUserUserPermissions(models.Model):
    id = models.AutoField()
    user_id = models.IntegerField()
    permission_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'


class BgworksAsyncwork(models.Model):
    id = models.AutoField()
    task_id = models.CharField(max_length=36)
    user_id = models.IntegerField()
    incarico_id = models.IntegerField(blank=True, null=True)
    tipo_doc = models.CharField(max_length=4)
    allegato_id = models.IntegerField(blank=True, null=True)
    content = models.CharField(max_length=512, blank=True, null=True)
    nuovo = models.BooleanField()
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    descrizione = models.CharField(max_length=255, blank=True, null=True)
    tstamp = models.DateTimeField(blank=True, null=True)
    hidden = models.BooleanField()

    class Meta:
        managed = False
        db_table = 'bgworks_asyncwork'


class CeleryTaskmeta(models.Model):
    id = models.AutoField()
    task_id = models.CharField(max_length=255)
    status = models.CharField(max_length=50)
    result = models.TextField(blank=True, null=True)
    date_done = models.DateTimeField()
    traceback = models.TextField(blank=True, null=True)
    hidden = models.BooleanField()
    meta = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'celery_taskmeta'


class CeleryTasksetmeta(models.Model):
    id = models.AutoField()
    taskset_id = models.CharField(max_length=255)
    result = models.TextField()
    date_done = models.DateTimeField()
    hidden = models.BooleanField()

    class Meta:
        managed = False
        db_table = 'celery_tasksetmeta'


class CommentiCommenti(models.Model):
    comment_ptr_id = models.IntegerField()
    destinatario_id = models.IntegerField(blank=True, null=True)
    gruppo_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'commenti_commenti'


class CommentiCommentogruppoletto(models.Model):
    id = models.AutoField()
    commento_id = models.IntegerField()
    gruppo_id = models.IntegerField()
    data_lettura = models.DateTimeField()
    ip_lettura = models.GenericIPAddressField()
    user_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'commenti_commentogruppoletto'


class CommentiCommentoletto(models.Model):
    id = models.AutoField()
    commento_id = models.IntegerField()
    user_id = models.IntegerField()
    data_lettura = models.DateTimeField()
    ip_lettura = models.GenericIPAddressField()

    class Meta:
        managed = False
        db_table = 'commenti_commentoletto'


class CommentiProprietagruppo(models.Model):
    id = models.AutoField()
    colore = models.CharField(max_length=7)
    coltxt = models.CharField(max_length=7)
    gruppo_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'commenti_proprietagruppo'


class ComuniComune(models.Model):
    id = models.AutoField()
    nome = models.CharField(max_length=255)
    cap = models.CharField(max_length=5)
    provincia_id = models.IntegerField()
    prefisso = models.CharField(max_length=6, blank=True, null=True)
    istat = models.CharField(max_length=6, blank=True, null=True)
    codfisco = models.CharField(max_length=4, blank=True, null=True)
    link = models.CharField(max_length=200, blank=True, null=True)
    fullname = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'comuni_comune'


class ComuniProvincia(models.Model):
    id = models.AutoField()
    codice = models.CharField(max_length=2)
    nome = models.CharField(max_length=255)
    regione_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'comuni_provincia'


class ComuniRegione(models.Model):
    id = models.AutoField()
    codice = models.CharField(max_length=3)
    nome = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'comuni_regione'


class ConteggiConteggio(models.Model):
    id = models.AutoField()
    user_id = models.IntegerField()
    data_fattura_gte = models.DateField(blank=True, null=True)
    data_fattura_lte = models.DateField(blank=True, null=True)
    data_fattura_year = models.IntegerField(blank=True, null=True)
    data_fattura_month = models.IntegerField(blank=True, null=True)
    data_fattura_day = models.IntegerField(blank=True, null=True)
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'conteggi_conteggio'


class ConteggiConteggioriga(models.Model):
    id = models.AutoField()
    sessione_id = models.IntegerField()
    fatturariga_id = models.IntegerField(blank=True, null=True)
    repertorio = models.CharField(max_length=10, blank=True, null=True)
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'conteggi_conteggioriga'


class CoreAgenzia(models.Model):
    id = models.AutoField()
    nome = models.CharField(max_length=255)
    compagnia1_id = models.IntegerField()
    fullname = models.CharField(max_length=255)
    codice = models.CharField(max_length=10)
    indirizzo = models.CharField(max_length=255)
    citta = models.CharField(max_length=255)
    cap = models.CharField(max_length=5)
    provincia = models.CharField(max_length=2)
    nazione = models.CharField(max_length=255)
    telefono = models.CharField(max_length=255)
    fax = models.CharField(max_length=255)
    email = models.CharField(max_length=75)
    piva = models.CharField(max_length=11)
    referente = models.CharField(max_length=255)
    nota = models.TextField()
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    comune_id = models.IntegerField(blank=True, null=True)
    sostituito_da_id = models.IntegerField(blank=True, null=True)
    sostituito_il = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'core_agenzia'


class CoreAgenziaperiti(models.Model):
    id = models.AutoField()
    agenzia_id = models.IntegerField()
    perito_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'core_agenziaperiti'


class CoreChecklist(models.Model):
    id = models.AutoField()
    capitolo = models.CharField(max_length=4)
    posizione = models.IntegerField(blank=True, null=True)
    domanda = models.CharField(max_length=255)
    necessaria = models.BooleanField()
    bloccante = models.BooleanField()
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'core_checklist'


class CoreCompagnia(models.Model):
    id = models.AutoField()
    nome = models.CharField(max_length=255)
    ania = models.CharField(max_length=4)
    indirizzo = models.CharField(max_length=512)
    citta = models.CharField(max_length=255)
    cap = models.CharField(max_length=5)
    provincia = models.CharField(max_length=2)
    nazione = models.CharField(max_length=255)
    piva = models.CharField(max_length=11)
    email = models.CharField(max_length=75)
    ggconsegna = models.IntegerField(blank=True, null=True)
    ggsopralluogo = models.IntegerField(blank=True, null=True)
    restituzione = models.CharField(max_length=2)
    url_restituzione = models.CharField(max_length=200)
    email_restituzione = models.CharField(max_length=75)
    atto_liquidazione = models.CharField(max_length=2)
    nota = models.TextField()
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    comune_id = models.IntegerField(blank=True, null=True)
    # Field name made lowercase.
    datasufoto = models.BooleanField(db_column='dataSuFoto')
    # Field name made lowercase.
    richiedeinterlocutoria = models.BooleanField(
        db_column='richiedeInterlocutoria')
    attiva = models.BooleanField()
    # Field name made lowercase.
    ggprimocontatto = models.IntegerField(
        db_column='ggprimoContatto', blank=True, null=True)
    # Field name made lowercase.
    gginterlocutoria = models.IntegerField(
        db_column='ggInterlocutoria', blank=True, null=True)
    ggattesamancatasottoscrizione = models.IntegerField(
        db_column='ggAttesaMancataSottoscrizione', blank=True, null=True)  # Field name made lowercase.
    # Field name made lowercase.
    limitepl = models.DecimalField(
        db_column='limitePL', max_digits=6, decimal_places=2)

    class Meta:
        managed = False
        db_table = 'core_compagnia'


class CoreCompagniaCollegate(models.Model):
    id = models.AutoField()
    from_compagnia_id = models.IntegerField()
    to_compagnia_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'core_compagnia_collegate'


class CoreCompagniaagenzie(models.Model):
    id = models.AutoField()
    compagnia_id = models.IntegerField()
    agenzia_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'core_compagniaagenzie'


class CoreComputo(models.Model):
    id = models.AutoField()
    incarico_id = models.IntegerField()
    posizione = models.IntegerField(blank=True, null=True)
    garanzia_id = models.IntegerField()
    partita_id = models.IntegerField()
    degrado_partita = models.IntegerField(blank=True, null=True)
    degrado_valore = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    limite_indennizzo = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    totnuovo = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    totale = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    totnuovoivato = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    totusoivato = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    importo_iva_nuovo = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    importo_iva_uso = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    degrado_ivato = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    totoriginale = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    importo_iva_originale = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    totoriginaleivato = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'core_computo'


class CoreComputoriga(models.Model):
    id = models.AutoField()
    computo_id = models.IntegerField()
    posizione = models.IntegerField(blank=True, null=True)
    voce = models.TextField(blank=True, null=True)
    quantita = models.DecimalField(max_digits=12, decimal_places=2)
    umisura = models.CharField(max_length=10, blank=True, null=True)
    prezzo = models.DecimalField(max_digits=12, decimal_places=2)
    degrado_voce = models.IntegerField(blank=True, null=True)
    degrado_valore = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    totnuovo = models.DecimalField(max_digits=12, decimal_places=2)
    totriga = models.DecimalField(max_digits=12, decimal_places=2)
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    aliquota_iva = models.IntegerField(blank=True, null=True)
    totnuovoivato = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    totrigaivato = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    degrado_ivato = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    tiporiga = models.CharField(max_length=2)

    class Meta:
        managed = False
        db_table = 'core_computoriga'


class CoreDocumento(models.Model):
    id = models.AutoField()
    incarico_id = models.IntegerField()
    tipo_id = models.IntegerField()
    descrizione = models.CharField(max_length=255)
    da_assicurato = models.BooleanField()
    content = models.CharField(max_length=512)
    data_documento = models.DateField()
    versione = models.IntegerField()
    nota = models.TextField()
    allegato_perizia = models.BooleanField()
    data_consegna = models.DateField(blank=True, null=True)
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    approvato = models.BooleanField()
    consegnato = models.BooleanField()
    messaggio_id = models.IntegerField(blank=True, null=True)
    posizione = models.IntegerField(blank=True, null=True)
    content_type = models.CharField(max_length=255, blank=True, null=True)
    macchina_fotografica = models.CharField(max_length=511)
    data_foto = models.DateField(blank=True, null=True)
    latitudine = models.DecimalField(
        max_digits=24, decimal_places=20, blank=True, null=True)
    longitudine = models.DecimalField(
        max_digits=24, decimal_places=20, blank=True, null=True)
    # Field name made lowercase.
    data_stampafoto = models.DateField(
        db_column='data_stampaFoto', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'core_documento'


class CoreFattura(models.Model):
    id = models.AutoField()
    numero_fattura_id = models.IntegerField()
    data_fattura = models.DateField()
    cliente_nome = models.CharField(max_length=255)
    cliente_indirizzo = models.CharField(max_length=512)
    cliente_citta = models.CharField(max_length=255)
    cliente_cap = models.CharField(max_length=5)
    cliente_provincia = models.CharField(max_length=2)
    cliente_piva = models.CharField(max_length=11)
    importo_onorario = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    importo_km = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    importo_foto = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    importo_varie = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    imponibile = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    aliquota_iva = models.IntegerField(blank=True, null=True)
    importo_iva = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    totale = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    compagnia_id = models.IntegerField(blank=True, null=True)
    chiusa = models.BooleanField()
    annotazioni = models.TextField()
    user_create = models.IntegerField()
    incarico_compagnia = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'core_fattura'


class CoreFatturanumero(models.Model):
    id = models.AutoField()
    organizzazione_id = models.IntegerField()
    numero = models.IntegerField()
    anno = models.IntegerField()
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    data_fattura = models.DateField(blank=True, null=True)
    user_create = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'core_fatturanumero'


class CoreFatturariga(models.Model):
    id = models.AutoField()
    fattura_id = models.IntegerField()
    repertorio = models.CharField(max_length=10)
    incarico_id = models.IntegerField(blank=True, null=True)
    sincro_netelenco = models.FloatField(blank=True, null=True)
    imponibile = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    onorario = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    onorario_foto = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    onorario_km = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    onorario_varie = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    perito_onorario = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    perito_onorario_foto = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    perito_onorario_km = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    codice_agenzia = models.CharField(max_length=10, blank=True, null=True)
    num_sinistro = models.CharField(max_length=255, blank=True, null=True)
    data_sinistro = models.DateField(blank=True, null=True)
    assicurato = models.CharField(max_length=255, blank=True, null=True)
    danneggiato1 = models.CharField(max_length=255, blank=True, null=True)
    num_polizza = models.CharField(max_length=255, blank=True, null=True)
    perito_imponibile = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    perito_id = models.IntegerField(blank=True, null=True)
    collaboratore = models.CharField(max_length=40)
    data_fattura = models.DateField(blank=True, null=True)
    organizzazione_id = models.IntegerField(blank=True, null=True)
    revisore_id = models.IntegerField(blank=True, null=True)
    revisore_imponibile = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    perito_provv_perc = models.DecimalField(
        max_digits=5, decimal_places=1, blank=True, null=True)
    data_incarico = models.DateField(blank=True, null=True)
    data_restituzione = models.DateField(blank=True, null=True)
    posizione = models.IntegerField(blank=True, null=True)
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    tipo = models.CharField(max_length=2)
    descrizione = models.CharField(max_length=512, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'core_fatturariga'


class CoreGaranzia(models.Model):
    id = models.AutoField()
    incarico_id = models.IntegerField()
    interessata = models.BooleanField()
    tipo_id = models.IntegerField()
    altro_tipo = models.CharField(max_length=255, blank=True, null=True)
    tipo_sinistro_id = models.IntegerField(blank=True, null=True)
    forma_copertura = models.CharField(max_length=3)
    franchigia = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    limite_perc = models.FloatField(blank=True, null=True)
    limite_valore = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    limite_min = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    limite_max = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    limite_anno_perc = models.FloatField(blank=True, null=True)
    limite_anno_valore = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    limite_anno_min = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    limite_anno_max = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    scoperto_perc = models.FloatField(blank=True, null=True)
    scoperto_valore = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    scoperto_min = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    scoperto_max = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    descrizione = models.TextField()
    certificata = models.BooleanField()
    certificatore_id = models.IntegerField(blank=True, null=True)
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    limite_perc_tipo = models.CharField(max_length=4, blank=True, null=True)
    limite_anno_perc_tipo = models.CharField(
        max_length=4, blank=True, null=True)
    somma_erogata_anno = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    salta_scopertura = models.BooleanField()
    posizione = models.IntegerField(blank=True, null=True)
    franchigia_tipo = models.CharField(max_length=2)

    class Meta:
        managed = False
        db_table = 'core_garanzia'


class CoreIncarico(models.Model):
    id = models.AutoField()
    repertorio = models.BigIntegerField()
    stato_id = models.CharField(max_length=2)
    compagnia_id = models.IntegerField(blank=True, null=True)
    agenzia_id = models.IntegerField()
    organizzazione_id = models.IntegerField()
    perito_id = models.IntegerField(blank=True, null=True)
    revisore_id = models.IntegerField(blank=True, null=True)
    assicurato_id = models.IntegerField()
    contraente_id = models.IntegerField(blank=True, null=True)
    danneggiato1_id = models.IntegerField(blank=True, null=True)
    danneggiato2_id = models.IntegerField(blank=True, null=True)
    danneggiato3_id = models.IntegerField(blank=True, null=True)
    danneggiato4_id = models.IntegerField(blank=True, null=True)
    ispettorato_id = models.IntegerField()
    data_sinistro = models.DateField(blank=True, null=True)
    data_denuncia = models.DateField(blank=True, null=True)
    data_assegnazione = models.DateField(blank=True, null=True)
    data_assegnazione_perito = models.DateField(blank=True, null=True)
    data_sopralluogo = models.DateField(blank=True, null=True)
    data_revisione = models.DateField(blank=True, null=True)
    data_restituzione = models.DateField(blank=True, null=True)
    data_fatturazione = models.DateField(blank=True, null=True)
    mancato_sopralluogo = models.CharField(max_length=4)
    ubicazione_rischio = models.TextField()
    tipo_sinistro_id = models.IntegerField()
    num_sinistro = models.CharField(max_length=255)
    indirizzo_sinistro = models.CharField(max_length=512)
    cap_sinistro = models.CharField(max_length=5)
    citta_sinistro = models.CharField(max_length=255)
    provincia_sinistro = models.CharField(max_length=2)
    nazione_sinistro = models.CharField(max_length=255)
    luogo_appuntamento = models.TextField()
    data_appuntamento = models.DateTimeField(blank=True, null=True)
    num_polizza = models.CharField(max_length=255)
    libretto_id = models.IntegerField(blank=True, null=True)
    riferimento = models.CharField(max_length=255)
    ramo_polizza_id = models.IntegerField(blank=True, null=True)
    pl_pagamento = models.CharField(max_length=4)
    coassicuratrici = models.TextField()
    sinistro_complesso = models.BooleanField()
    vincoli = models.BooleanField()
    riserve = models.BooleanField()
    tipo_atto = models.CharField(max_length=2)
    contatto_persona = models.CharField(max_length=255)
    contatto_telefono = models.CharField(max_length=255)
    data_primo_contatto = models.DateTimeField(blank=True, null=True)
    data_effetto = models.DateField(blank=True, null=True)
    data_scadenza_premio = models.DateField(blank=True, null=True)
    data_pagamento_premio = models.DateField(blank=True, null=True)
    data_scadenza_copertura = models.DateField(blank=True, null=True)
    durata_copertura = models.CharField(max_length=2)
    franchigia_frontale = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    deroga_globale = models.IntegerField(blank=True, null=True)
    tipo_deroga = models.CharField(max_length=4)
    danno_nuovo = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    danno_lordo = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    danno_indennizzabile = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    danno_arrotondato = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    liq_calcolata_stima = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    liq_calcolata_supplemento = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    liq_calcolata_totale = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    liq_arrotondata_stima = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    liq_arrotondata_supplemento = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    liq_arrotondata_totale = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    onorario = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    perito_provv_fisso = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    perito_provv_perc = models.DecimalField(
        max_digits=5, decimal_places=1, blank=True, null=True)
    revisore_provv_fisso = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    revisore_provv_perc = models.DecimalField(
        max_digits=5, decimal_places=1, blank=True, null=True)
    provv_perito = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    provv_revisore = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    compenso = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    onorario_foto = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    onorario_km = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    onorario_varie = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    azienda_fattura_id = models.IntegerField(blank=True, null=True)
    onorario_km_percorsi = models.IntegerField(blank=True, null=True)
    onorario_km_franchigia = models.IntegerField(blank=True, null=True)
    onorario_km_costo = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    onorario_foto_numero = models.IntegerField(blank=True, null=True)
    onorario_foto_franchigia = models.IntegerField(blank=True, null=True)
    onorario_foto_costo = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    perito_onorario_foto_numero = models.IntegerField(blank=True, null=True)
    perito_onorario_foto_franchigia = models.IntegerField(
        blank=True, null=True)
    perito_onorario_foto_costo = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    perito_onorario_foto = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    perito_onorario_km_percorsi = models.IntegerField(blank=True, null=True)
    perito_onorario_km_franchigia = models.IntegerField(blank=True, null=True)
    perito_onorario_km_costo = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    perito_onorario_km = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    imponibile = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    perito_imponibile = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    perito_onorario = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    revisore_imponibile = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    riconosci_iva = models.BooleanField()
    riserva_importo = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    danno_originale = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    danneggiato5_id = models.IntegerField(blank=True, null=True)
    comune_sinistro_id = models.IntegerField(blank=True, null=True)
    aperto_pl = models.CharField(max_length=2)
    attesa = models.CharField(max_length=4)
    scadenza_attesa = models.DateField(blank=True, null=True)
    incarico_compagnia = models.CharField(
        max_length=255, blank=True, null=True)
    regolarita_amministrativa = models.CharField(
        max_length=2, blank=True, null=True)
    user_register_id = models.IntegerField(blank=True, null=True)
    user_deliver_id = models.IntegerField(blank=True, null=True)
    data_registrazione = models.DateField(blank=True, null=True)
    iban = models.CharField(max_length=34)
    iban_intestatario = models.CharField(max_length=255)
    latitudine = models.DecimalField(
        max_digits=24, decimal_places=20, blank=True, null=True)
    longitudine = models.DecimalField(
        max_digits=24, decimal_places=20, blank=True, null=True)
    data_chiusura = models.DateField(blank=True, null=True)
    data_archiviazione = models.DateField(blank=True, null=True)
    rivalsa = models.BooleanField()
    note = models.TextField(blank=True, null=True)
    riparazione_diretta = models.BooleanField()
    video_perizia = models.BooleanField()

    class Meta:
        managed = False
        db_table = 'core_incarico'


class CoreIspettorato(models.Model):
    id = models.AutoField()
    nome = models.CharField(max_length=255)
    compagnia_id = models.IntegerField()
    fullname = models.CharField(max_length=255)
    codice = models.CharField(max_length=10)
    indirizzo = models.CharField(max_length=255)
    citta = models.CharField(max_length=255)
    cap = models.CharField(max_length=5)
    provincia = models.CharField(max_length=2)
    nazione = models.CharField(max_length=255)
    telefono = models.CharField(max_length=255)
    fax = models.CharField(max_length=255)
    email = models.CharField(max_length=75)
    piva = models.CharField(max_length=11)
    referente = models.CharField(max_length=255)
    intestazione = models.CharField(max_length=4)
    nota = models.TextField()
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    comune_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'core_ispettorato'


class CoreLibretto(models.Model):
    id = models.AutoField()
    compagnia_id = models.IntegerField()
    content = models.CharField(max_length=512)
    nome = models.CharField(max_length=255)
    modello = models.CharField(max_length=255)
    versione = models.CharField(max_length=255)
    fullname = models.CharField(max_length=255)
    annotazioni = models.TextField(blank=True, null=True)
    franchigia_frontale = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    deroga_perc = models.IntegerField(blank=True, null=True)
    tipo_deroga = models.CharField(max_length=4)
    certificatore_id = models.IntegerField()
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    content_type = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'core_libretto'


class CoreLibrettogaranzia(models.Model):
    id = models.AutoField()
    libretto_id = models.IntegerField()
    tipogaranzia_id = models.IntegerField()
    tipo_sinistro_id = models.IntegerField(blank=True, null=True)
    forma_copertura = models.CharField(max_length=3)
    franchigia = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    limite_perc = models.FloatField(blank=True, null=True)
    limite_valore = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    limite_min = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    limite_max = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    limite_anno_perc = models.FloatField(blank=True, null=True)
    limite_anno_valore = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    limite_anno_min = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    limite_anno_max = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    scoperto_perc = models.IntegerField(blank=True, null=True)
    scoperto_valore = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    scoperto_min = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    scoperto_max = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    descrizione = models.TextField()
    certificatore_id = models.IntegerField()
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    salta_scopertura = models.BooleanField()
    altro_tipogaranzia = models.CharField(
        max_length=255, blank=True, null=True)
    limite_perc_tipo = models.CharField(max_length=4, blank=True, null=True)
    limite_anno_perc_tipo = models.CharField(
        max_length=4, blank=True, null=True)
    franchigia_tipo = models.CharField(max_length=2)

    class Meta:
        managed = False
        db_table = 'core_librettogaranzia'


class CoreLiquidazione(models.Model):
    id = models.AutoField()
    incarico_id = models.IntegerField()
    garanzia_id = models.IntegerField()
    franchigia = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    totale_nuovo = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    totale_detrarre = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    totale_stima = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    totale_supplemento = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    totale_riga = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    scoperto_stima = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    scoperto_supplemento = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    scoperto_riga = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    importo_stima = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    importo_supplemento = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    importo_riga = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    totale_degrado = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    totale_proporzionale = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    importo_calcolato = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    importo_limite = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    limite_applicato = models.BooleanField()
    tot_liquidazione_altre = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    importo_limite_stima = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'core_liquidazione'


class CoreLiquidazioneriga(models.Model):
    id = models.AutoField()
    liquidazione_id = models.IntegerField()
    posizione = models.IntegerField(blank=True, null=True)
    partita_id = models.IntegerField(blank=True, null=True)
    computo_id = models.IntegerField(blank=True, null=True)
    stima_nuovo = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    stima_degrado = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    stima_uso = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    stima_stato = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    somma_limite = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    valore_liquidazione = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    valore_supplemento = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    valore_proporzionale = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    totale_riga = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    totale_calcolato = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    limite_applicato = models.BooleanField()

    class Meta:
        managed = False
        db_table = 'core_liquidazioneriga'


class CoreMessaggio(models.Model):
    id = models.AutoField()
    incarico_id = models.IntegerField()
    subject = models.CharField(max_length=255)
    message_id = models.CharField(max_length=255)
    from_header = models.CharField(max_length=255)
    to_header = models.TextField()
    processed = models.DateTimeField(blank=True, null=True)
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    testo = models.TextField()
    result = models.TextField()

    class Meta:
        managed = False
        db_table = 'core_messaggio'


class CoreModello(models.Model):
    id = models.AutoField()
    capitolo = models.CharField(max_length=4)
    titolo = models.CharField(max_length=255)
    testo = models.TextField()
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    posizione = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'core_modello'


class CoreModulo(models.Model):
    id = models.AutoField()
    compagnia_id = models.IntegerField(blank=True, null=True)
    content = models.CharField(max_length=512)
    data_documento = models.DateField()
    versione = models.IntegerField()
    nota = models.TextField()
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    tiposinistro_id = models.IntegerField(blank=True, null=True)
    unikey = models.TextField()
    tipodoc_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'core_modulo'


class CoreNonconforme(models.Model):
    id = models.AutoField()
    data_respinta = models.DateTimeField(blank=True, null=True)
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    revisione_id = models.IntegerField(blank=True, null=True)
    motivazione = models.TextField()
    utente_respintore_id = models.IntegerField(blank=True, null=True)
    stato_nonconformita_id = models.CharField(
        max_length=2, blank=True, null=True)
    incarico_id = models.IntegerField(blank=True, null=True)
    utente_respinto_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'core_nonconforme'


class CoreOrganizzazione(models.Model):
    id = models.AutoField()
    codice = models.CharField(max_length=10)
    nome = models.CharField(max_length=255)
    indirizzo = models.CharField(max_length=255, blank=True, null=True)
    citta = models.CharField(max_length=255)
    cap = models.CharField(max_length=5)
    provincia = models.CharField(max_length=2)
    nazione = models.CharField(max_length=255)
    telefono = models.CharField(max_length=255)
    fax = models.CharField(max_length=255)
    email = models.CharField(max_length=75)
    piva = models.CharField(max_length=11)
    referente = models.CharField(max_length=255)
    coordinatore_id = models.IntegerField()
    nota = models.TextField()
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    org_provv_perc = models.DecimalField(max_digits=5, decimal_places=1)
    emissione_fattura = models.BooleanField()
    posizione = models.IntegerField(blank=True, null=True)
    is_active = models.BooleanField()
    comune_id = models.IntegerField(blank=True, null=True)
    verifica_perizie = models.BooleanField()

    class Meta:
        managed = False
        db_table = 'core_organizzazione'


class CoreOrganizzazioneCollaboratori(models.Model):
    id = models.AutoField()
    organizzazione_id = models.IntegerField()
    user_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'core_organizzazione_collaboratori'


class CoreParametro(models.Model):
    id = models.AutoField()
    codice = models.CharField(max_length=64)
    valore = models.CharField(max_length=512, blank=True, null=True)
    descrizione = models.TextField()
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'core_parametro'


class CorePartita(models.Model):
    id = models.AutoField()
    incarico_id = models.IntegerField()
    tipo_id = models.IntegerField()
    valore_ax_iniziale = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    valore_ax_indicizzato = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    valore_ax = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    valore_nuovo = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    deroga_non_applicata = models.BooleanField()
    deroga_perc = models.IntegerField(blank=True, null=True)
    deroga_valore = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    scopertura = models.BooleanField()
    scopertura_ratio = models.FloatField(blank=True, null=True)
    degrado = models.IntegerField(blank=True, null=True)
    valore_degrado = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    valore_uso = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    preesistenza_dichiarata = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    perc_danno = models.IntegerField(blank=True, null=True)
    forma_copertura = models.CharField(max_length=3)
    metodo_liquidazione = models.CharField(max_length=3)
    descrizione = models.CharField(max_length=255)
    colpita = models.BooleanField()
    preesistenza_opt = models.CharField(max_length=1)
    costruzione = models.CharField(max_length=50)
    costr_anno = models.IntegerField(blank=True, null=True)
    manutenzione = models.CharField(max_length=50)
    finiture = models.CharField(max_length=50)
    solai = models.CharField(max_length=50)
    piani_ft = models.IntegerField(blank=True, null=True)
    piani_int = models.IntegerField(blank=True, null=True)
    data_stima = models.DateField(blank=True, null=True)
    ubicazione_polizza = models.BooleanField()
    descrizione_polizza = models.BooleanField()
    destinazione = models.TextField()
    strutture = models.TextField()
    danno_accertato = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    danno_supplemento = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    danno_lordo = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    fabbricato_l1 = models.FloatField(blank=True, null=True)
    fabbricato_l2 = models.FloatField(blank=True, null=True)
    fabbricato_l3 = models.FloatField(blank=True, null=True)
    cubatura = models.FloatField(blank=True, null=True)
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    posizione = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'core_partita'


class CorePartitariga(models.Model):
    id = models.AutoField()
    partita_id = models.IntegerField()
    posizione = models.IntegerField(blank=True, null=True)
    descrizione = models.TextField(blank=True, null=True)
    fabbricato_l1 = models.FloatField(blank=True, null=True)
    fabbricato_l2 = models.FloatField(blank=True, null=True)
    fabbricato_l3 = models.FloatField(blank=True, null=True)
    fabbricato_cubatura = models.FloatField(blank=True, null=True)
    costo_mc = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    quantita = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    umisura = models.CharField(max_length=10, blank=True, null=True)
    prezzo_uni = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    valore_nuovo = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    degrado_riga = models.IntegerField(blank=True, null=True)
    valore_degrado = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    valore_uso = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'core_partitariga'


class CorePersona(models.Model):
    id = models.AutoField()
    titolo_id = models.IntegerField(blank=True, null=True)
    nome = models.CharField(max_length=255)
    giuridica = models.BooleanField()
    ruolo = models.CharField(max_length=3)
    cod_fiscale = models.CharField(max_length=16)
    piva = models.CharField(max_length=11)
    indirizzo = models.CharField(max_length=512)
    cap = models.CharField(max_length=5)
    citta = models.CharField(max_length=255)
    provincia = models.CharField(max_length=2)
    nazione = models.CharField(max_length=255)
    telefono1 = models.CharField(max_length=255)
    telefono2 = models.CharField(max_length=255)
    email = models.CharField(max_length=75)
    idcard = models.CharField(max_length=50)
    riferimento = models.CharField(max_length=255)
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    comune_id = models.IntegerField(blank=True, null=True)
    amministratore_id = models.IntegerField(blank=True, null=True)
    iban = models.CharField(max_length=34)
    sostituito_da_id = models.IntegerField(blank=True, null=True)
    sostituito_il = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'core_persona'


class CorePrezziario(models.Model):
    id = models.AutoField()
    tipoprezziario_id = models.IntegerField()
    nome = models.CharField(max_length=255)
    voce = models.TextField(blank=True, null=True)
    umisura = models.CharField(max_length=3)
    prezzo = models.DecimalField(max_digits=12, decimal_places=2)
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'core_prezziario'


class CoreQuestionario(models.Model):
    id = models.AutoField()
    revisione_id = models.IntegerField(blank=True, null=True)
    capitolo = models.CharField(max_length=4)
    posizione = models.IntegerField(blank=True, null=True)
    domanda = models.CharField(max_length=255)
    necessaria = models.BooleanField()
    bloccante = models.BooleanField()
    risposta = models.BooleanField()
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'core_questionario'


class CoreRamo(models.Model):
    id = models.AutoField()
    descrizione = models.CharField(max_length=255)
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'core_ramo'


class CoreRelazione(models.Model):
    id = models.AutoField()
    incarico_id = models.IntegerField()
    sezione01 = models.TextField()
    sezione02 = models.TextField()
    sezione03 = models.TextField()
    sezione04 = models.TextField()
    sezione05 = models.TextField()
    sezione06 = models.TextField()
    sezione07 = models.TextField()
    sezione08 = models.TextField()
    sezione09 = models.TextField()
    sezione10 = models.TextField()
    sezione11 = models.TextField()
    sezione12 = models.TextField()
    foglio_liquidazione = models.TextField()
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'core_relazione'


class CoreRevisione(models.Model):
    id = models.AutoField()
    incarico_id = models.IntegerField()
    sezione01 = models.TextField()
    sezione02 = models.TextField()
    sezione03 = models.TextField()
    sezione04 = models.TextField()
    sezione05 = models.TextField()
    sezione06 = models.TextField()
    sezione07 = models.TextField()
    sezione08 = models.TextField()
    sezione09 = models.TextField()
    sezione10 = models.TextField()
    sezione11 = models.TextField()
    sezione12 = models.TextField()
    foglio_liquidazione = models.TextField()
    commenti01 = models.TextField()
    commenti02 = models.TextField()
    commenti03 = models.TextField()
    commenti04 = models.TextField()
    commenti05 = models.TextField()
    commenti06 = models.TextField()
    commenti07 = models.TextField()
    commenti08 = models.TextField()
    commenti09 = models.TextField()
    commenti10 = models.TextField()
    commenti11 = models.TextField()
    commenti12 = models.TextField()
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    revisore_attuale_id = models.IntegerField(blank=True, null=True)
    revisore_apertura = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'core_revisione'


class CoreSopralluogo(models.Model):
    id = models.AutoField()
    incarico_id = models.IntegerField()
    # Field name made lowercase.
    personasopralluogo = models.CharField(
        db_column='personaSopralluogo', max_length=255)
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'core_sopralluogo'


class CoreStatigruppo(models.Model):
    id = models.AutoField()
    stato_id = models.IntegerField()
    gruppo_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'core_statigruppo'


class CoreStato(models.Model):
    id = models.AutoField()
    codice = models.CharField(max_length=2)
    descrizione = models.CharField(max_length=50)
    next_stato = models.CharField(max_length=2)
    colore = models.CharField(max_length=7)
    coltxt = models.CharField(max_length=7)
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'core_stato'


class CoreTipodoc(models.Model):
    id = models.AutoField()
    codice = models.CharField(max_length=4)
    descrizione = models.CharField(max_length=50)
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    colore = models.CharField(max_length=7)
    modulo = models.BooleanField()
    corrispondenza = models.BooleanField()
    allegabile = models.BooleanField()
    parole_chiave = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'core_tipodoc'


class CoreTipogaranzia(models.Model):
    id = models.AutoField()
    codice = models.CharField(max_length=6)
    nome = models.CharField(max_length=255)
    descrizione = models.TextField()
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'core_tipogaranzia'


class CoreTipopartita(models.Model):
    id = models.AutoField()
    codice = models.CharField(max_length=4)
    nome = models.CharField(max_length=255)
    descrizione = models.TextField(blank=True, null=True)
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'core_tipopartita'


class CoreTipoprezziario(models.Model):
    id = models.AutoField()
    nome = models.CharField(max_length=255)
    descrizione = models.TextField(blank=True, null=True)
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'core_tipoprezziario'


class CoreTiposinistro(models.Model):
    id = models.AutoField()
    codice = models.CharField(max_length=4)
    descrizione = models.CharField(max_length=255)
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    istruzioni = models.TextField()

    class Meta:
        managed = False
        db_table = 'core_tiposinistro'


class CoreTitolo(models.Model):
    id = models.AutoField()
    titolo = models.CharField(max_length=15)
    descrizione = models.CharField(max_length=64)
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'core_titolo'


class CoreUserprofile(models.Model):
    id = models.AutoField()
    user_id = models.IntegerField()
    titolo_id = models.IntegerField(blank=True, null=True)
    indirizzo = models.CharField(max_length=512)
    cap = models.CharField(max_length=5)
    citta = models.CharField(max_length=255)
    nazione = models.CharField(max_length=255)
    telefono1 = models.CharField(max_length=255)
    telefono2 = models.CharField(max_length=255)
    piva = models.CharField(max_length=11)
    cod_fiscale = models.CharField(max_length=16)
    perito_capacita = models.IntegerField(blank=True, null=True)
    perito_carico = models.IntegerField(blank=True, null=True)
    perito_provv_fisso = models.DecimalField(max_digits=12, decimal_places=2)
    perito_provv_perc = models.DecimalField(max_digits=5, decimal_places=1)
    revisore_provv_fisso = models.DecimalField(max_digits=12, decimal_places=2)
    revisore_provv_perc = models.DecimalField(max_digits=5, decimal_places=1)
    img_firma = models.CharField(max_length=512, blank=True, null=True)
    liquidato_medio = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    elaborato_medio = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    email2 = models.CharField(max_length=75)
    email3 = models.CharField(max_length=75)
    iban = models.CharField(max_length=34)
    cattolica_userid = models.CharField(max_length=64)

    class Meta:
        managed = False
        db_table = 'core_userprofile'


class DjangoAdminLog(models.Model):
    id = models.AutoField()
    action_time = models.DateTimeField()
    user_id = models.IntegerField()
    content_type_id = models.IntegerField(blank=True, null=True)
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoCommentFlags(models.Model):
    id = models.AutoField()
    user_id = models.IntegerField()
    comment_id = models.IntegerField()
    flag = models.CharField(max_length=30)
    flag_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_comment_flags'


class DjangoComments(models.Model):
    id = models.AutoField()
    content_type_id = models.IntegerField()
    object_pk = models.TextField()
    site_id = models.IntegerField()
    user_id = models.IntegerField(blank=True, null=True)
    user_name = models.CharField(max_length=50)
    user_email = models.CharField(max_length=254)
    user_url = models.CharField(max_length=200)
    comment = models.TextField()
    submit_date = models.DateTimeField()
    ip_address = models.GenericIPAddressField(blank=True, null=True)
    is_public = models.BooleanField()
    is_removed = models.BooleanField()

    class Meta:
        managed = False
        db_table = 'django_comments'


class DjangoContentType(models.Model):
    id = models.AutoField()
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'


class DjangoDbstorageFile(models.Model):
    name = models.CharField(max_length=512)
    data = models.TextField()
    size = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'django_dbstorage_file'


class DjangoMailboxMailbox(models.Model):
    id = models.AutoField()
    name = models.CharField(max_length=255)
    uri = models.CharField(max_length=255, blank=True, null=True)
    active = models.BooleanField()
    from_email = models.CharField(max_length=255, blank=True, null=True)
    last_polling = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'django_mailbox_mailbox'


class DjangoMailboxMessage(models.Model):
    id = models.AutoField()
    mailbox_id = models.IntegerField()
    subject = models.CharField(max_length=255)
    message_id = models.CharField(max_length=255)
    body = models.TextField()
    processed = models.DateTimeField()
    outgoing = models.BooleanField()
    in_reply_to_id = models.IntegerField(blank=True, null=True)
    from_header = models.CharField(max_length=255)
    to_header = models.TextField()
    read = models.DateTimeField(blank=True, null=True)
    encoded = models.BooleanField()
    eml = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'django_mailbox_message'


class DjangoMailboxMessageattachment(models.Model):
    id = models.AutoField()
    document = models.CharField(max_length=100)
    message_id = models.IntegerField(blank=True, null=True)
    headers = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'django_mailbox_messageattachment'


class DjangoMigrations(models.Model):
    id = models.AutoField()
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class DjangoSite(models.Model):
    id = models.AutoField()
    domain = models.CharField(max_length=100)
    name = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'django_site'


class DjceleryCrontabschedule(models.Model):
    id = models.AutoField()
    minute = models.CharField(max_length=64)
    hour = models.CharField(max_length=64)
    day_of_week = models.CharField(max_length=64)
    day_of_month = models.CharField(max_length=64)
    month_of_year = models.CharField(max_length=64)

    class Meta:
        managed = False
        db_table = 'djcelery_crontabschedule'


class DjceleryIntervalschedule(models.Model):
    id = models.AutoField()
    every = models.IntegerField()
    period = models.CharField(max_length=24)

    class Meta:
        managed = False
        db_table = 'djcelery_intervalschedule'


class DjceleryPeriodictask(models.Model):
    id = models.AutoField()
    name = models.CharField(max_length=200)
    task = models.CharField(max_length=200)
    interval_id = models.IntegerField(blank=True, null=True)
    crontab_id = models.IntegerField(blank=True, null=True)
    args = models.TextField()
    kwargs = models.TextField()
    queue = models.CharField(max_length=200, blank=True, null=True)
    exchange = models.CharField(max_length=200, blank=True, null=True)
    routing_key = models.CharField(max_length=200, blank=True, null=True)
    expires = models.DateTimeField(blank=True, null=True)
    enabled = models.BooleanField()
    last_run_at = models.DateTimeField(blank=True, null=True)
    total_run_count = models.IntegerField()
    date_changed = models.DateTimeField()
    description = models.TextField()

    class Meta:
        managed = False
        db_table = 'djcelery_periodictask'


class DjceleryPeriodictasks(models.Model):
    ident = models.SmallIntegerField()
    last_update = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'djcelery_periodictasks'


class DjceleryTaskstate(models.Model):
    id = models.AutoField()
    state = models.CharField(max_length=64)
    task_id = models.CharField(max_length=36)
    name = models.CharField(max_length=200, blank=True, null=True)
    tstamp = models.DateTimeField()
    args = models.TextField(blank=True, null=True)
    kwargs = models.TextField(blank=True, null=True)
    eta = models.DateTimeField(blank=True, null=True)
    expires = models.DateTimeField(blank=True, null=True)
    result = models.TextField(blank=True, null=True)
    traceback = models.TextField(blank=True, null=True)
    runtime = models.FloatField(blank=True, null=True)
    retries = models.IntegerField()
    worker_id = models.IntegerField(blank=True, null=True)
    hidden = models.BooleanField()

    class Meta:
        managed = False
        db_table = 'djcelery_taskstate'


class DjceleryWorkerstate(models.Model):
    id = models.AutoField()
    hostname = models.CharField(max_length=255)
    last_heartbeat = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'djcelery_workerstate'


class EasyThumbnailsSource(models.Model):
    id = models.AutoField()
    name = models.CharField(max_length=255)
    modified = models.DateTimeField()
    storage_hash = models.CharField(max_length=40)

    class Meta:
        managed = False
        db_table = 'easy_thumbnails_source'


class EasyThumbnailsThumbnail(models.Model):
    id = models.AutoField()
    name = models.CharField(max_length=255)
    modified = models.DateTimeField()
    source_id = models.IntegerField()
    storage_hash = models.CharField(max_length=40)

    class Meta:
        managed = False
        db_table = 'easy_thumbnails_thumbnail'


class EasyThumbnailsThumbnaildimensions(models.Model):
    id = models.AutoField()
    thumbnail_id = models.IntegerField()
    width = models.IntegerField(blank=True, null=True)
    height = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'easy_thumbnails_thumbnaildimensions'


class InterlocutorieInterlocutoria(models.Model):
    id = models.AutoField()
    incarico_id = models.IntegerField()
    data_documento = models.DateField()
    tipo = models.CharField(max_length=4)
    sezione01 = models.TextField()
    sezione02 = models.TextField()
    sezione03 = models.TextField()
    sezione04 = models.TextField()
    data_consegna = models.DateField(blank=True, null=True)
    posizione = models.IntegerField(blank=True, null=True)
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    riserva_valore = models.DecimalField(
        max_digits=12, decimal_places=2, blank=True, null=True)
    gallery = models.BooleanField()
    stato = models.CharField(max_length=2)
    formato = models.CharField(max_length=3)
    documento_id = models.IntegerField(blank=True, null=True)
    esterno = models.BooleanField()

    class Meta:
        managed = False
        db_table = 'interlocutorie_interlocutoria'


class InterlocutorieModello(models.Model):
    id = models.AutoField()
    capitolo = models.CharField(max_length=4)
    titolo = models.CharField(max_length=255)
    testo = models.TextField()
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)
    posizione = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'interlocutorie_modello'


class LockingLock(models.Model):
    id = models.AutoField()
    locked_at = models.DateTimeField(blank=True, null=True)
    app = models.CharField(max_length=255, blank=True, null=True)
    model = models.CharField(max_length=255, blank=True, null=True)
    entry_id = models.IntegerField()
    locked_by = models.IntegerField(blank=True, null=True)
    hard_lock = models.BooleanField()

    class Meta:
        managed = False
        db_table = 'locking_lock'


class LuoghiLuogoincarico(models.Model):
    id = models.AutoField()
    incarico_id = models.IntegerField()
    campo = models.CharField(max_length=3)
    indirizzo = models.CharField(max_length=255, blank=True, null=True)
    geocode = models.CharField(max_length=255)
    latitudine = models.DecimalField(
        max_digits=24, decimal_places=20, blank=True, null=True)
    longitudine = models.DecimalField(
        max_digits=24, decimal_places=20, blank=True, null=True)
    zoom = models.DecimalField(
        max_digits=8, decimal_places=4, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'luoghi_luogoincarico'


class OnlineUsersOnlineuseractivity(models.Model):
    id = models.AutoField()
    last_activity = models.DateTimeField()
    user_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'online_users_onlineuseractivity'


class ReportBuilderDisplayfield(models.Model):
    id = models.AutoField()
    report_id = models.IntegerField()
    path = models.CharField(max_length=2000)
    path_verbose = models.CharField(max_length=2000)
    field = models.CharField(max_length=2000)
    field_verbose = models.CharField(max_length=2000)
    name = models.CharField(max_length=2000)
    sort = models.IntegerField(blank=True, null=True)
    sort_reverse = models.BooleanField()
    width = models.IntegerField()
    aggregate = models.CharField(max_length=5)
    position = models.SmallIntegerField(blank=True, null=True)
    total = models.BooleanField()
    group = models.BooleanField()
    display_format_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'report_builder_displayfield'


class ReportBuilderFilterfield(models.Model):
    id = models.AutoField()
    report_id = models.IntegerField()
    path = models.CharField(max_length=2000)
    path_verbose = models.CharField(max_length=2000)
    field = models.CharField(max_length=2000)
    field_verbose = models.CharField(max_length=2000)
    filter_type = models.CharField(max_length=20)
    filter_value = models.CharField(max_length=2000)
    filter_value2 = models.CharField(max_length=2000)
    exclude = models.BooleanField()
    position = models.SmallIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'report_builder_filterfield'


class ReportBuilderFormat(models.Model):
    id = models.AutoField()
    name = models.CharField(max_length=50)
    string = models.CharField(max_length=300)

    class Meta:
        managed = False
        db_table = 'report_builder_format'


class ReportBuilderReport(models.Model):
    id = models.AutoField()
    name = models.CharField(max_length=255)
    root_model_id = models.IntegerField()
    created = models.DateField()
    modified = models.DateField()
    distinct = models.BooleanField()
    slug = models.CharField(max_length=50)
    user_created_id = models.IntegerField(blank=True, null=True)
    user_modified_id = models.IntegerField(blank=True, null=True)
    description = models.TextField()
    report_file = models.CharField(max_length=100)
    report_file_creation = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'report_builder_report'


class ReportBuilderReportStarred(models.Model):
    id = models.AutoField()
    report_id = models.IntegerField()
    user_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'report_builder_report_starred'


class RequestRequest(models.Model):
    id = models.AutoField()
    response = models.SmallIntegerField()
    method = models.CharField(max_length=7)
    path = models.CharField(max_length=255)
    time = models.DateTimeField()
    is_secure = models.BooleanField()
    is_ajax = models.BooleanField()
    ip = models.GenericIPAddressField()
    user_id = models.IntegerField(blank=True, null=True)
    referer = models.CharField(max_length=255, blank=True, null=True)
    user_agent = models.CharField(max_length=255, blank=True, null=True)
    language = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'request_request'


class SouthMigrationhistory(models.Model):
    id = models.AutoField()
    app_name = models.CharField(max_length=255)
    migration = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'south_migrationhistory'


class TastypieApiaccess(models.Model):
    id = models.AutoField()
    identifier = models.CharField(max_length=255)
    url = models.CharField(max_length=255)
    request_method = models.CharField(max_length=10)
    accessed = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'tastypie_apiaccess'


class TastypieApikey(models.Model):
    id = models.AutoField()
    user_id = models.IntegerField()
    key = models.CharField(max_length=256)
    created = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'tastypie_apikey'


class WsmanagerWserver(models.Model):
    id = models.AutoField()
    compagnia_id = models.IntegerField()
    url_wsdl = models.CharField(max_length=200)
    is_active = models.BooleanField()
    user = models.CharField(max_length=255, blank=True, null=True)
    password = models.CharField(max_length=255, blank=True, null=True)
    wsclass = models.CharField(max_length=255)
    date_edit = models.DateTimeField()
    user_edit = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'wsmanager_wserver'
