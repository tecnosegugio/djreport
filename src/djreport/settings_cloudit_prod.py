# -*- coding: UTF-8 -*-
"""
Copyright (c) 2011-2019 TONIOLO PIERPAOLO, All rights reserved.
Use of this source code is governed by a BSD-style license that can be
found in the LICENSE file.

This file is part of Propilei project
-------------------------------------
Created on 10 gen 2019

@author: tecnosegugio
"""

from .settings import *  # @UnusedWildImport

DEBUG = False

DEBUG_TOOLBAR = False

ALLOWED_HOSTS = ['reportbuilder.trentin.com', 'localhost']

# 3 = sito di produzione
SITE_ID = 3

INTERNAL_IPS = ('127.0.0.1', )
