# -*- coding: utf-8 -*-
"""
Copyright (c) 2011-2019 TONIOLO PIERPAOLO, All rights reserved.
Use of this source code is governed by a BSD-style license that can be
found in the LICENSE file.

This file is part of Propilei project
-------------------------------------
Created on 9 gen 2019

@author: tecnosegugio
"""


class PropileiRouter(object):
    """
    A router to control all database operations on models in the
    auth application.
    """

    def db_for_read(self, model, **hints):
        """
        Legge i dati di Propilei solo dal database Propilei
        """
        if model._meta.app_label == 'propilei':
            return 'propilei'
        return None

    def db_for_write(self, model, **hints):
        """
        Scrive i dati di Propilei solo sul database Propilei
        """
        if model._meta.app_label == 'propilei':
            return 'propilei'
        return None

    def allow_relation(self, obj1, obj2, **hints):
        """
        Accetta la creazione di relazioni tra tabelle di Propilei
        solo se sono ambedue di Propilei
        """
        if obj1._meta.app_label == 'propilei' or \
           obj2._meta.app_label == 'propilei':
            return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        """
        Impedisce le migrazioni sul database di Propilei
        """
        if app_label == 'propilei':
            return False
        return None
