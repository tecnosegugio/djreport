# -*- coding: UTF-8 -*-
"""
Copyright (c) 2011-2019 TONIOLO PIERPAOLO, All rights reserved.
Use of this source code is governed by a BSD-style license that can be
found in the LICENSE file.

This file is part of Propilei project
-------------------------------------
Created on 11 gen 2019

@author: tecnosegugio
"""

import logging

from django.http import UnreadablePostError
from django.utils.translation import ugettext as _
from django.utils.encoding import force_str


logger = logging.getLogger(__name__)


def skip_unreadable_post(record):
    if record.exc_info:
        exc_type, exc_value = record.exc_info[:2]  # @UnusedVariable
        logger.warning(_(u'skip_undreadable_post: eccezione %s: %s') % (
            exc_type,
            force_str(exc_value),
        ))
        if isinstance(exc_value, UnreadablePostError):
            return False
    return True
